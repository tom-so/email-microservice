﻿using EmailService.Services.EmailDetailValidators;
using FluentAssertions;
using NUnit.Framework;

namespace EmailService.Tests.Unit.Services.EmailDetailValidators
{
    public class EmailAddressValidatorTests
    {
        [TestCase(null)]
        [TestCase("")]
        [TestCase("   ")]
        public void WhenEmailIsNullOrWhitespace_ThenInvalid(string emailAddress)
        {
            var validator = new EmailAddressValidator();

            bool valid = validator.Validate(emailAddress);

            valid.Should().BeFalse();
        }

        [TestCase("test@gmail")]
        [TestCase("test.gmail")]
        [TestCase("testgmail")]
        public void WhenEmailHasNotEitherDotOrAt_ThenInvalid(string emailAddress)
        {
            var validator = new EmailAddressValidator();

            bool valid = validator.Validate(emailAddress);

            valid.Should().BeFalse();
        }

        [Test]
        public void WhenEmailHasDotAndAt_ThenValid()
        {
            var validator = new EmailAddressValidator();

            bool valid = validator.Validate("test@gmail.com");

            valid.Should().BeTrue();
        }
    }
}
