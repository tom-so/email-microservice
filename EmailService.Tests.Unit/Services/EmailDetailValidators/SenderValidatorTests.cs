﻿using EmailService.Messages;
using EmailService.ResultObjects;
using EmailService.Services.EmailDetailValidators;
using FluentAssertions;
using Moq;
using NUnit.Framework;

namespace EmailService.Tests.Unit.Services.EmailDetailValidators
{
    public class SenderValidatorTests
    {
        [Test]
        public void WhenSenderHasIncorrectEmail_ThenInvalid()
        {
            var email = new EmailDto { Sender = "testelocom" };
            var validator = new SenderValidator(
                Mock.Of<IEmailAddressValidator>(v =>
                    v.Validate(It.IsAny<string>()) == false));

            EmailValidationDetailResult result = validator.ValidateDetail(email);

            result.Success.Should().BeFalse();
            result.ValidationMessage.Should().NotBeEmpty();
        }

        [Test]
        public void WhenSenderHasValidEmail_ThenValid()
        {
            var email = new EmailDto { Sender = "test@elo.com" };
            var validator = new SenderValidator(
                Mock.Of<IEmailAddressValidator>(v => v.Validate(It.IsAny<string>()) == true));

            EmailValidationDetailResult result = validator.ValidateDetail(email);

            result.Success.Should().BeTrue();
            result.ValidationMessage.Should().BeNull();
        }
    }
}
