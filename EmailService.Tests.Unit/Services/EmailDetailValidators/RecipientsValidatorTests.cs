﻿using EmailService.Messages;
using EmailService.ResultObjects;
using EmailService.Services.EmailDetailValidators;
using FluentAssertions;
using Moq;
using NUnit.Framework;

namespace EmailService.Tests.Unit.Services.EmailDetailValidators
{
    public class RecipientsValidatorTests
    {
        private static readonly object[] MissingRecipients =
        {
            new EmailDto { Recipients = null },
            new EmailDto { Recipients = new RecipientDto[0] }
        };

        [TestCaseSource(nameof(MissingRecipients))]
        public void WhenRecipientsCollectionIsNullOrEmpty_ThenInvalid(EmailDto email)
        {
            var validator = new RecipientsValidator(
                Mock.Of<IEmailAddressValidator>(v => v.Validate(It.IsAny<string>()) == true));

            EmailValidationDetailResult result = validator.ValidateDetail(email);

            result.Success.Should().BeFalse();
            result.ValidationMessage.Should().NotBeEmpty();
        }

        [Test]
        public void WhenAnyRecipientHasIncorrectEmail_ThenInvalid()
        {
            var email = new EmailDto
            {
                Recipients = new[] 
                {
                    new RecipientDto { EmailAddress = "test@test.pl" },
                    new RecipientDto { EmailAddress = "" }
                }
            };
            var validator = new RecipientsValidator(
                Mock.Of<IEmailAddressValidator>(v => 
                    v.Validate(It.Is<string>(s => s == "")) == false &&
                    v.Validate(It.Is<string>(s => s == "test@test.pl")) == true));

            EmailValidationDetailResult result = validator.ValidateDetail(email);

            result.Success.Should().BeFalse();
            result.ValidationMessage.Should().NotBeEmpty();
        }

        [Test]
        public void WhenAllRecipientsHasValidEmail_ThenInvalid()
        {
            var email = new EmailDto
            {
                Recipients = new[]
                {
                    new RecipientDto { EmailAddress = "test@test.pl" },
                    new RecipientDto { EmailAddress = "test2@testing.com" }
                }
            };
            var validator = new RecipientsValidator(
                Mock.Of<IEmailAddressValidator>(v =>v.Validate(It.IsAny<string>()) == true));

            EmailValidationDetailResult result = validator.ValidateDetail(email);

            result.Success.Should().BeTrue();
            result.ValidationMessage.Should().BeNull();
        }
    }
}
