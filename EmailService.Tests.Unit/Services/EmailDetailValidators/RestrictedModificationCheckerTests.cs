﻿using EmailService.Entities;
using EmailService.Services.EmailDetailValidators;
using FluentAssertions;
using NUnit.Framework;

namespace EmailService.Tests.Unit.Services.EmailDetailValidators
{
    public class RestrictedModificationCheckerTests
    {
        [Test]
        public void WhenEmailHasStatusDifferentThanSent_ThenModificationIsAllowed()
        {
            var email = new Email { Status = SendingStatus.Pending };
            var checker = new RestrictedModificationChecker();

            bool isModificationAllowed = checker.IsModificationAllowed(email);

            isModificationAllowed.Should().BeTrue();
        }

        [Test]
        public void WhenEmailHasSentStatus_ThenModificationIsNotAllowed()
        {
            var email = new Email { Status = SendingStatus.Sent };
            var checker = new RestrictedModificationChecker();

            bool isModificationAllowed = checker.IsModificationAllowed(email);

            isModificationAllowed.Should().BeFalse();
        }
    }
}
