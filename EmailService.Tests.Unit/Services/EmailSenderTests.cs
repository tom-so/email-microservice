﻿using EmailService.Entities;
using EmailService.Repositories;
using EmailService.Services;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using System;
using System.Threading.Tasks;

namespace EmailService.Tests.Unit.Services
{
    public class EmailSenderTests
    {
        [Test]
        public void SendSingle_WhenSmtpServerThrowsException_ThenDelegateException()
        {
            var smtpSender = Mock.Of<ISmtpEmailSender>();
            Mock.Get(smtpSender).Setup(s => s.SendViaSmtp(It.IsAny<Email>())).Throws<Exception>();
            var emailSender = new EmailSender(
                Mock.Of<IEmailModificationRepository>(),
                smtpSender);

            emailSender
                .Invoking(async es => await es.Send(new Email()))
                .Should()
                .Throw<Exception>();
        }

        [Test]
        public void SendSingle_WhenCannotChangeEmailStatus_ThenDelegateException()
        {
            var repository = Mock.Of<IEmailModificationRepository>();
            Mock.Get(repository)
                .Setup(r => r.ModifyStatus("123", SendingStatus.Sent))
                .Throws<Exception>();
            var emailSender = new EmailSender(repository, Mock.Of<ISmtpEmailSender>());

            emailSender
                .Invoking(async es => await es.Send(new Email { Id = "123" }))
                .Should()
                .Throw<Exception>();
        }

        [Test]
        public async Task SendSingle_WhenEmailIsSuccessfullySentAndTheStatusIsChanged_ThenReturnSuccessWithChangedElement()
        {
            var repository = Mock.Of<IEmailModificationRepository>();
            Mock.Get(repository)
                .Setup(r => r.ModifyStatus("123", SendingStatus.Sent))
                .Returns(Task.FromResult(new Email()));
            var smtpSender = Mock.Of<ISmtpEmailSender>();
            Mock.Get(smtpSender)
                .Setup(s => s.SendViaSmtp(It.IsAny<Email>()))
                .Returns(Task.CompletedTask);
            var emailSender = new EmailSender(repository, smtpSender);

            var result = await emailSender.Send(new Email { Id = "123" });

            result.Success.Should().BeTrue();
            result.SentEmails.Should().ContainSingle();
        }

        [Test]
        public async Task SendMultiple_WhenEmailIsSuccessfullySentAndTheStatusIsChanged_ThenReturnSuccessWithChangedElement()
        {
            var repository = Mock.Of<IEmailModificationRepository>();
            Mock.Get(repository)
                .Setup(r => r.ModifyStatus(It.IsAny<string>(), SendingStatus.Sent))
                .Returns(Task.FromResult(new Email()));
            var smtpSender = Mock.Of<ISmtpEmailSender>();
            Mock.Get(smtpSender)
                .Setup(s => s.SendViaSmtp(It.IsAny<Email>()))
                .Returns(Task.CompletedTask);
            var emailSender = new EmailSender(repository, smtpSender);

            var result = await emailSender.Send(
                new[] 
                { 
                    new Email { Id = "123" },
                    new Email { Id = "345" }
                });

            result.Success.Should().BeTrue();
            result.SentEmails.Should().HaveCount(2);
        }
    }
}
