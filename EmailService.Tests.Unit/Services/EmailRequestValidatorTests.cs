﻿using EmailService.Messages;
using EmailService.ResultObjects;
using EmailService.Services;
using FluentAssertions;
using Moq;
using NUnit.Framework;

namespace EmailService.Tests.Unit.Services
{
    public class EmailRequestValidatorTests
    {
        [Test]
        public void Validate_WhenAllDetailValidatorsAreReturningSuccess_ThenEmailRequestIsValid()
        {
            var emailDetailValidators = new[] 
            { 
                Mock.Of<IEmailDetailValidator>(v => 
                    v.ValidateDetail(It.IsAny<EmailDto>()) == new EmailValidationDetailResult { Success = true }), 
                Mock.Of<IEmailDetailValidator>(v => 
                    v.ValidateDetail(It.IsAny<EmailDto>()) == new EmailValidationDetailResult { Success = true }) 
            };
            var validator = new EmailRequestValidator(emailDetailValidators);

            EmailValidationResult result = validator.Validate(new EmailDto());

            result.Success.Should().BeTrue();
        }

        [Test]
        public void Validate_WhenSomeValidatorsAreReturningIssues_ThenInvalidAndAllErrorsAreReturned()
        {
            var emailDetailValidators = new[]
            {
                Mock.Of<IEmailDetailValidator>(v =>
                    v.ValidateDetail(It.IsAny<EmailDto>()) == 
                        new EmailValidationDetailResult { Success = false, ValidationMessage = "Error2" }),
                Mock.Of<IEmailDetailValidator>(v =>
                    v.ValidateDetail(It.IsAny<EmailDto>()) == new EmailValidationDetailResult { Success = true }),
                Mock.Of<IEmailDetailValidator>(v =>
                    v.ValidateDetail(It.IsAny<EmailDto>()) == 
                        new EmailValidationDetailResult { Success = false, ValidationMessage = "Error1" })
            };
            var validator = new EmailRequestValidator(emailDetailValidators);

            EmailValidationResult result = validator.Validate(new EmailDto());

            result.Success.Should().BeFalse();
            result.ValidationMessages
                .Should()
                .NotBeEmpty().And
                .Contain("Error2").And
                .Contain("Error1");
        }

        [TestCase(null)]
        [TestCase("")]
        [TestCase("   ")]
        [TestCase("wed23")]
        public void ValidateId_WhenIdIsNullOrWhitespaceOrCannotBeParsed_ThenInvalid(string id)
        {
            var validator = new EmailRequestValidator(new IEmailDetailValidator[0]);

            bool result = validator.ValidateId(id);

            result.Should().BeFalse();
        }

        [Test]
        public void ValidateId_WhenIdIsAbleToBeParsed_ThenValid()
        {
            var validator = new EmailRequestValidator(new IEmailDetailValidator[0]);

            bool result = validator.ValidateId("5e4948786539f47fe8ade8df");

            result.Should().BeTrue();
        }
    }
}
