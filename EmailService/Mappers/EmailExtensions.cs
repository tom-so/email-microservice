﻿using EmailService.Entities;
using EmailService.Messages;
using System.Linq;
using System.Net.Mail;

namespace EmailService.Mappers
{
    public static class EmailExtensions
    {
        public static EmailDto ToEmailResponse(this Email email)
        {
            return new EmailDto
            {
                Id = email.Id,
                Sender = email.Sender,
                Recipients = email.Recipients.Select(r => new RecipientDto { EmailAddress = r.EmailAddress }).ToArray(),
                Content = email.Content,
                Status = email.Status.ToString()
            };
        }

        public static StatusDto ToStatusResponse(this Email email)
        {
            return new StatusDto 
            { 
                Id = email.Id,
                Status = email.Status.ToString() 
            };
        }

        public static MailMessage ToMailMessage(this Email email)
        { 
            return new MailMessage(
                from: email.Sender,
                to: string.Join(',', email.Recipients.Select(r => r.EmailAddress)),
                subject: "Company subject",
                body: email.Content);
        }
    }
}
