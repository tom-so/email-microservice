﻿using EmailService.Entities;
using EmailService.Messages;
using System.Linq;

namespace EmailService.Mappers
{
    public static class EmailDtoExtensions
    {
        public static Email ToEmailEntity(this EmailDto emailRequest, SendingStatus sendingStatus)
        {
            return new Email
            {
                Id = emailRequest.Id,
                Content = emailRequest.Content,
                Sender = emailRequest.Sender,
                Recipients = emailRequest.Recipients.Select(r => new Recipient { EmailAddress = r.EmailAddress }).ToArray(),
                Status = sendingStatus
            };
        }
    }
}
