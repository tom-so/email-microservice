﻿using EmailService.Messages;
using EmailService.ResultObjects;
using System.Linq;

namespace EmailService.Mappers
{
    public static class EmailValidationResultExtensions
    {
        public static ErrorsDto ToErrorResponse(this EmailValidationResult errorValidationResult)
        {
            return new ErrorsDto 
            { 
                Errors = errorValidationResult.ValidationMessages?.Select(m => new ErrorDto { Message = m }).ToArray()
                    ?? new[] { new ErrorDto { Message = "UnknownError" } }
            };
        }
    }
}
