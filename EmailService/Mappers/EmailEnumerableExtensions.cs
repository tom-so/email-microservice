﻿using EmailService.Entities;
using EmailService.Messages;
using System.Collections.Generic;
using System.Linq;

namespace EmailService.Mappers
{
    public static class EmailEnumerableExtensions
    {
        public static EmailsDto ToEmailsResponse(this IEnumerable<Email> emails)
        {
            return new EmailsDto { Emails = emails.Select(e => e.ToEmailResponse()).ToArray() };
        }
    }
}
