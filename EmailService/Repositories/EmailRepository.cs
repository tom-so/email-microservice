﻿using EmailService.Entities;
using EmailService.Providers;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmailService.Repositories
{
    public class EmailRepository : IEmailRepository
    {
        private readonly IMongoCollection<Email> _collection;

        public EmailRepository(IEmailsCollectionProvider databaseConnectionProvider)
        {
            _collection = databaseConnectionProvider.Get();
        }

        public Task Add(Email email)
        {
            try
            {
                return _collection.InsertOneAsync(email);
            }
            catch (MongoException)
            {
                // logging or sth
                throw;
            }
        }

        public Task<Email> Get(string id)
        {
            try
            {
                return _collection.Find(Builders<Email>.Filter.Eq("_id", ObjectId.Parse(id))).SingleOrDefaultAsync();
            }
            catch (MongoException)
            {
                // logging or sth
                throw;
            }
        }

        public Task<List<Email>> GetAll()
        {
            try
            {
                return _collection.Find(Builders<Email>.Filter.Empty).ToListAsync();
            }
            catch (MongoException)
            {
                // logging or sth
                throw;
            }
        }

        public Task<List<Email>> GetEmailsByStatus(SendingStatus status)
        {
            try
            {
                return _collection.Find(Builders<Email>.Filter.Eq("status", (int)status)).ToListAsync();
            }
            catch (MongoException)
            {
                // logging or sth
                throw;
            }
        }
    }
}
