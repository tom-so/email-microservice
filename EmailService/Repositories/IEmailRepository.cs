﻿using EmailService.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmailService.Repositories
{
    public interface IEmailRepository
    {
        Task Add(Email email);
        Task<Email> Get(string id);
        Task<List<Email>> GetAll();
        Task<List<Email>> GetEmailsByStatus(SendingStatus status);
    }
}
