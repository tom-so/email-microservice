﻿using EmailService.Entities;
using EmailService.Providers;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Threading.Tasks;

namespace EmailService.Repositories
{
    public class EmailModificationRepository : IEmailModificationRepository
    {
        private readonly IMongoCollection<Email> _collection;

        public EmailModificationRepository(IEmailsCollectionProvider emailsCollectionProvider)
        {
            _collection = emailsCollectionProvider.Get();
        }

        public Task<Email> Modify(string id, Email email)
        {
            try
            {
                return _collection.FindOneAndReplaceAsync(
                    filter: Builders<Email>.Filter.Eq("_id", ObjectId.Parse(id)),
                    replacement: email);
            }
            catch (MongoException)
            {
                // log me
                throw;
            }
        }

        public Task<Email> ModifyStatus(string id, SendingStatus sendingStatus)
        {
            try
            {
                return _collection.FindOneAndUpdateAsync(
                    filter: Builders<Email>.Filter.Eq("_id", ObjectId.Parse(id)),
                    update: Builders<Email>.Update.Set("Status", (int)sendingStatus));
            }
            catch (MongoException)
            {
                // log me, please please please
                throw;
            }
        }
    }
}
