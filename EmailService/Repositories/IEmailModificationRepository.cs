﻿using EmailService.Entities;
using System.Threading.Tasks;

namespace EmailService.Repositories
{
    public interface IEmailModificationRepository
    {
        Task<Email> ModifyStatus(string id, SendingStatus sendingStatus);
        Task<Email> Modify(string id, Email email);
    }
}
