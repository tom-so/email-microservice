using EmailService.Entities;
using EmailService.Middlewares;
using EmailService.Providers;
using EmailService.Repositories;
using EmailService.Services;
using EmailService.Services.EmailDetailValidators;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;

namespace EmailService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddSingleton<IEmailsCollectionProvider, EmailsCollectionProvider>();
            services.AddTransient<IEmailRepository, EmailRepository>();
            services.AddTransient<IEmailModificationRepository, EmailModificationRepository>();
            services.AddTransient<IEmailRequestValidator, EmailRequestValidator>(
                services => new EmailRequestValidator(services.GetServices<IEmailDetailValidator>()));
            services.AddTransient<IEmailSender, EmailSender>();
            services.AddTransient<IEmailAddressValidator, EmailAddressValidator>();
            services.AddTransient<IEmailDetailValidator, RecipientsValidator>();
            services.AddTransient<IEmailDetailValidator, SenderValidator>();
            services.AddTransient<IEmailDetailValidator, StatusValidator>();
            services.AddTransient<IRestrictedModificationChecker, RestrictedModificationChecker>();
            services.AddTransient<ISmtpEmailSender, SmtpEmailSender>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseMiddleware<ExceptionMiddleware>();

            BsonClassMap.RegisterClassMap<Email>(email =>
            {
                email.AutoMap();
                email.MapMember(e => e.Status).SetSerializer(new EnumSerializer<SendingStatus>(BsonType.Int32));
            });
            BsonClassMap.RegisterClassMap<Recipient>(recipient =>
            {
                recipient.AutoMap();
            });
        }
    }
}
