﻿using EmailService.Entities;
using System.Threading.Tasks;

namespace EmailService.Services
{
    public interface ISmtpEmailSender
    {
        Task SendViaSmtp(Email email);
    }
}
