﻿using EmailService.Messages;
using EmailService.ResultObjects;
using System;

namespace EmailService.Services.EmailDetailValidators
{
    public class SenderValidator : IEmailDetailValidator
    {
        private readonly IEmailAddressValidator _emailAddressValidator;

        public SenderValidator(IEmailAddressValidator emailAddressValidator)
        {
            _emailAddressValidator = emailAddressValidator ?? throw new ArgumentNullException(nameof(emailAddressValidator));
        }

        public EmailValidationDetailResult ValidateDetail(EmailDto email)
        {
            bool success = _emailAddressValidator.Validate(email.Sender);
            string errorMessage = success ? null : "Incorrect sender.";

            return new EmailValidationDetailResult { Success = success, ValidationMessage = errorMessage };
        }
    }
}
