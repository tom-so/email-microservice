﻿using EmailService.Entities;

namespace EmailService.Services.EmailDetailValidators
{
    public interface IRestrictedModificationChecker
    {
        bool IsModificationAllowed(Email email);
    }
}
