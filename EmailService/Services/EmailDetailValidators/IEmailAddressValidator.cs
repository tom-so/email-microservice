﻿namespace EmailService.Services.EmailDetailValidators
{
    public interface IEmailAddressValidator
    {
        bool Validate(string emailAddress);
    }
}
