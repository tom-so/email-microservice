﻿using EmailService.Entities;

namespace EmailService.Services.EmailDetailValidators
{
    public class RestrictedModificationChecker : IRestrictedModificationChecker
    {
        public bool IsModificationAllowed(Email email) => email.Status != SendingStatus.Sent;
    }
}
