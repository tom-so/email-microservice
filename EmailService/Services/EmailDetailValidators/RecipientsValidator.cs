﻿using EmailService.Messages;
using EmailService.ResultObjects;
using System;
using System.Linq;

namespace EmailService.Services.EmailDetailValidators
{
    public class RecipientsValidator : IEmailDetailValidator
    {
        private readonly IEmailAddressValidator _emailAddressValidator;

        public RecipientsValidator(IEmailAddressValidator emailAddressValidator)
        {
            _emailAddressValidator = emailAddressValidator ?? throw new ArgumentNullException(nameof(emailAddressValidator));
        }

        public EmailValidationDetailResult ValidateDetail(EmailDto email)
        {
            if (email.Recipients == null || !email.Recipients.Any())
            {
                return new EmailValidationDetailResult { Success = false, ValidationMessage = "Recipients are not provided." };
            }

            bool success = email.Recipients.All(r => _emailAddressValidator.Validate(r.EmailAddress));
            string errorMessage = success ? null : "Same of the provided recipients had incorrect email address.";

            return new EmailValidationDetailResult { Success = success, ValidationMessage = errorMessage };
        }
    }
}
