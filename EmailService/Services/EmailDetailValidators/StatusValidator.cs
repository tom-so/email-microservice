﻿using EmailService.Messages;
using EmailService.ResultObjects;

namespace EmailService.Services.EmailDetailValidators
{
    public class StatusValidator : IEmailDetailValidator
    {
        public EmailValidationDetailResult ValidateDetail(EmailDto email)
        {
            bool success = email.Status == null;
            string errorMessage = success ? null : "You are not allowed to force message status. Use appropriate endpoint.";

            return new EmailValidationDetailResult { Success = success, ValidationMessage = errorMessage };
        }
    }
}
