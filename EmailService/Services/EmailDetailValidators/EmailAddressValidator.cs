﻿namespace EmailService.Services.EmailDetailValidators
{
    public class EmailAddressValidator : IEmailAddressValidator
    {
        public bool Validate(string emailAddress)
        {
            if (string.IsNullOrWhiteSpace(emailAddress)) return false;

            return emailAddress.Contains('@') && emailAddress.Contains('.'); // naive way...
        }
    }
}
