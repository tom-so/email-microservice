﻿using EmailService.Messages;
using EmailService.ResultObjects;

namespace EmailService.Services
{
    public interface IEmailDetailValidator
    {
        EmailValidationDetailResult ValidateDetail(EmailDto email);
    }
}
