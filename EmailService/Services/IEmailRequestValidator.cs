﻿using EmailService.Messages;
using EmailService.ResultObjects;

namespace EmailService.Services
{
    public interface IEmailRequestValidator
    {
        EmailValidationResult Validate(EmailDto email);
        bool ValidateId(string id);
    }
}
