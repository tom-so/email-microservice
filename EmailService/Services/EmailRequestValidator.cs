﻿using EmailService.Entities;
using EmailService.Messages;
using EmailService.ResultObjects;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EmailService.Services
{
    public class EmailRequestValidator : IEmailRequestValidator
    {
        private readonly IEnumerable<IEmailDetailValidator> _emailDetailValidators;

        public EmailRequestValidator(IEnumerable<IEmailDetailValidator> emailDetailValidators)
        {
            _emailDetailValidators = emailDetailValidators ?? throw new ArgumentNullException(nameof(emailDetailValidators));
        }

        public EmailValidationResult Validate(EmailDto email)
        {
            if (email == null)
            {
                return new EmailValidationResult { Success = false, ValidationMessages = new[] { "Missing object." } };
            }

            var validationDetailMessages = _emailDetailValidators
                .Select(v => v.ValidateDetail(email))
                .Where(v => v.Success == false)
                .Select(result => result.ValidationMessage)
                .ToArray();

            return new EmailValidationResult
            {
                Success = !validationDetailMessages.Any(),
                ValidationMessages = validationDetailMessages
            };
        }

        public bool ValidateId(string id)
        {
            if (string.IsNullOrWhiteSpace(id)) return false;

            return ObjectId.TryParse(id, out _);
        }
    }
}
