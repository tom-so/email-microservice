﻿using EmailService.Entities;
using EmailService.ResultObjects;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmailService.Services
{
    public interface IEmailSender
    {
        Task<SendingEmailResult> Send(Email email);
        Task<SendingEmailResult> Send(IEnumerable<Email> emails);
    }
}
