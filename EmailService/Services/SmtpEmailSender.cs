﻿using EmailService.Entities;
using EmailService.Mappers;
using System;
using System.Net.Mail;
using System.Threading.Tasks;

namespace EmailService.Services
{
    public class SmtpEmailSender : ISmtpEmailSender
    {
        public Task SendViaSmtp(Email email)
        {
            SmtpClient client = new SmtpClient("company-server"); // move to config
            client.UseDefaultCredentials = true;

            try
            {
                return client.SendMailAsync(email.ToMailMessage());
            }
            catch (Exception)
            {
                // log
                throw;
            }
        }
    }
}
