﻿using EmailService.Entities;
using EmailService.Repositories;
using EmailService.ResultObjects;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmailService.Services
{
    public class EmailSender : IEmailSender
    {
        private readonly IEmailModificationRepository _emailRepository;
        private readonly ISmtpEmailSender _smtpEmailSender;

        public EmailSender(
            IEmailModificationRepository emailModificationRepository,
            ISmtpEmailSender smtpEmailSender)
        {
            _emailRepository = emailModificationRepository ?? throw new ArgumentNullException(nameof(emailModificationRepository));
            _smtpEmailSender = smtpEmailSender ?? throw new ArgumentNullException(nameof(smtpEmailSender));
        }

        public async Task<SendingEmailResult> Send(Email email)
        {
            await _smtpEmailSender.SendViaSmtp(email);
            Email sentEmail = await ChangeStatusToSent(email);

            return new SendingEmailResult { Success = true, SentEmails = new[] { sentEmail } };
        }

        public async Task<SendingEmailResult> Send(IEnumerable<Email> emails)
        {
            var sentEmails = new List<Email>();
            foreach (Email email in emails)
            {
                await _smtpEmailSender.SendViaSmtp(email);
                Email modifiedStatusEmail = await ChangeStatusToSent(email);
                sentEmails.Add(modifiedStatusEmail);
            }

            return new SendingEmailResult { Success = true, SentEmails = sentEmails.ToArray() };
        }

        private Task<Email> ChangeStatusToSent(Email email)
        {
            return _emailRepository.ModifyStatus(email.Id, SendingStatus.Sent);
        }
    }
}
