﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace EmailService.Entities
{
    public class Email
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        public string Sender { get; set; }

        public Recipient[] Recipients { get; set; }

        public string Content { get; set; }

        [BsonRepresentation(BsonType.Int32)]
        public SendingStatus Status { get; set; }
    }
}
