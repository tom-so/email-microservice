﻿namespace EmailService.Entities
{
    public class Recipient
    {
        public string EmailAddress { get; set; }
    }
}
