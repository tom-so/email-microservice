﻿namespace EmailService.Entities
{
    public enum SendingStatus
    {
        Unknown = 0,
        Pending = 2,
        Sent = 4
    }
}
