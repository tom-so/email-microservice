﻿namespace EmailService.ResultObjects
{
    public class EmailValidationResult
    {
        public bool Success { get; set; }
        public string[] ValidationMessages { get; set; }
    }
}
