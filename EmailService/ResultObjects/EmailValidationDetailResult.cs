﻿namespace EmailService.ResultObjects
{
    public class EmailValidationDetailResult
    {
        public bool Success { get; set; }
        public string ValidationMessage { get; set; }
    }
}
