﻿using EmailService.Entities;

namespace EmailService.ResultObjects
{
    public class SendingEmailResult
    {
        public bool Success { get; set; }

        public Email[] SentEmails { get; set; }
    }
}
