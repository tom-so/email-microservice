﻿using EmailService.Messages;

namespace EmailService.Factories
{
    public static class ErrorFactory
    {
        public static ErrorsDto CreateObjectDoesNotExistError()
        { 
            return new ErrorsDto { Errors = new[] { new ErrorDto { Message = "Object does not exist." } } };
        }

        public static ErrorsDto CreateInvalidIdError()
        {
            return new ErrorsDto { Errors = new[] { new ErrorDto { Message = "Invalid object id." } } };
        }

        public static ErrorsDto CreateInternalServerError()
        {
            return new ErrorsDto { Errors = new[] { new ErrorDto { Message = "Internal server error." } } };
        }

        public static ErrorsDto CreateAlreadySentEmailError()
        {
            return new ErrorsDto { Errors = new[] { new ErrorDto { Message = "The email is already sent." } } };
        }
    }
}
