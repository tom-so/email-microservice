﻿using EmailService.Entities;
using MongoDB.Driver;

namespace EmailService.Providers
{
    public class EmailsCollectionProvider : IEmailsCollectionProvider
    {
        private IMongoCollection<Email> emailsCollection;

        public IMongoCollection<Email> Get()
        {
            if (emailsCollection == null)
            {
                emailsCollection = new MongoClient().GetDatabase("Emails").GetCollection<Email>("EmailDetails");
            }

            return emailsCollection;
        }
    }
}
