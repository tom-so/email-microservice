﻿using EmailService.Entities;
using MongoDB.Driver;

namespace EmailService.Providers
{
    public interface IEmailsCollectionProvider
    {
        IMongoCollection<Email> Get();
    }
}
