﻿namespace EmailService.Messages
{
    public class ErrorDto
    { 
        public string Message { get; set; }
    }
}
