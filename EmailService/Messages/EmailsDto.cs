﻿namespace EmailService.Messages
{
    public class EmailsDto
    {
        public EmailDto[] Emails { get; set; }
    }
}
