﻿namespace EmailService.Messages
{
    public class StatusDto
    {
        public string Id { get; set; }
        public string Status { get; set; }
    }
}
