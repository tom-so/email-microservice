﻿namespace EmailService.Messages
{
    public class EmailDto
    {
        public string Id { get; set; }
        public string Sender { get; set; }
        public RecipientDto[] Recipients { get; set; }
        public string Content { get; set; }
        public string Status { get; set; }
    }
}
