﻿namespace EmailService.Messages
{
    public class RecipientDto
    {
        public string EmailAddress { get; set; }
    }
}
