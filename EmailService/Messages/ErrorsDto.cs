﻿namespace EmailService.Messages
{
    public class ErrorsDto
    {
        public ErrorDto[] Errors { get; set; }
    }
}
