﻿using EmailService.Factories;
using Microsoft.AspNetCore.Mvc;

namespace EmailService.Controllers
{
    public abstract class EmailBaseController : ControllerBase
    {
        protected NotFoundObjectResult EmailNotFound()
        {
            return NotFound(ErrorFactory.CreateObjectDoesNotExistError());
        }

        protected BadRequestObjectResult InvalidEmailId()
        {
            return BadRequest(ErrorFactory.CreateInvalidIdError());
        }

        protected ObjectResult InternalServerError()
        {
            return StatusCode(500, ErrorFactory.CreateInternalServerError());
        }
    }
}