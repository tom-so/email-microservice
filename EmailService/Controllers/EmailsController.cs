﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EmailService.Entities;
using EmailService.Factories;
using EmailService.Mappers;
using EmailService.Repositories;
using EmailService.ResultObjects;
using EmailService.Services;
using Microsoft.AspNetCore.Mvc;

namespace EmailService.Controllers
{
    [ApiController]
    [Route("api/emails")]
    public class EmailsController : ControllerBase
    {
        private readonly IEmailRepository _emailRepository;
        private readonly IEmailSender _emailSender;

        public EmailsController(
            IEmailRepository emailRepository,
            IEmailSender emailSender)
        {
            _emailRepository = emailRepository ?? throw new ArgumentNullException(nameof(emailRepository));
            _emailSender = emailSender ?? throw new ArgumentNullException(nameof(emailSender));
        }

        [HttpGet]
        public async Task<IActionResult> GetAllEmailsInTheSystem()
        {
            List<Email> allEmails = await _emailRepository.GetAll();

            return Ok(allEmails.ToEmailsResponse());
        }

        [HttpPost("sendAllPending")]
        public async Task<IActionResult> SendAllPendingEmails()
        {
            List<Email> allPendingEmails = await _emailRepository.GetEmailsByStatus(SendingStatus.Pending);

            SendingEmailResult sendingEmailResult = await _emailSender.Send(allPendingEmails);
            if (!sendingEmailResult.Success) return StatusCode(500, ErrorFactory.CreateInternalServerError());

            return Ok(allPendingEmails.ToEmailsResponse());
        }
    }
}
