﻿using System;
using EmailService.Entities;
using EmailService.Factories;
using EmailService.Services.EmailDetailValidators;
using Microsoft.AspNetCore.Mvc;

namespace EmailService.Controllers
{
    public abstract class EmailModificationBaseController : EmailBaseController
    {
        private readonly IRestrictedModificationChecker _restrictedModificationChecker;

        protected EmailModificationBaseController(IRestrictedModificationChecker restrictedModificationChecker)
        {
            _restrictedModificationChecker = restrictedModificationChecker ?? throw new ArgumentNullException(nameof(restrictedModificationChecker));
        }

        protected BadRequestObjectResult AlreadySentError()
        {
            return BadRequest(ErrorFactory.CreateInternalServerError());
        }

        protected bool IsModificationRestricted(Email email)
        {
            return !_restrictedModificationChecker.IsModificationAllowed(email);
        }
    }
}