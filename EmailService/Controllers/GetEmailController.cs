﻿using System.Threading.Tasks;
using EmailService.Entities;
using EmailService.Mappers;
using EmailService.Repositories;
using EmailService.Services;
using Microsoft.AspNetCore.Mvc;

namespace EmailService.Controllers
{
    [Route("api/email")]
    [ApiController]
    public class GetEmailController : EmailBaseController
    {
        private readonly IEmailRepository _emailRepository;
        private readonly IEmailRequestValidator _emailValidator;

        public GetEmailController(
            IEmailRepository emailRepository,
            IEmailRequestValidator emailValidator)
        {
            _emailRepository = emailRepository ?? throw new System.ArgumentNullException(nameof(emailRepository));
            _emailValidator = emailValidator ?? throw new System.ArgumentNullException(nameof(emailValidator));
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetEmail(string id)
        {
            if (!_emailValidator.ValidateId(id)) return InvalidEmailId();

            Email email = await _emailRepository.Get(id);
            if (email == null) return EmailNotFound();

            return Ok(email.ToEmailResponse());
        }

        [HttpGet("{id}/status")]
        public async Task<IActionResult> GetStatus(string id)
        {
            if (!_emailValidator.ValidateId(id)) return InvalidEmailId();

            Email email = await _emailRepository.Get(id);
            if (email == null) return EmailNotFound();

            return Ok(email.ToStatusResponse());
        }
    }
}