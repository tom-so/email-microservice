﻿using System;
using System.Threading.Tasks;
using EmailService.Entities;
using EmailService.Mappers;
using EmailService.Repositories;
using EmailService.ResultObjects;
using EmailService.Services;
using EmailService.Services.EmailDetailValidators;
using Microsoft.AspNetCore.Mvc;

namespace EmailService.Controllers
{
    [Route("api/email")]
    [ApiController]
    public class SendEmailController : EmailModificationBaseController
    {
        private readonly IEmailRepository _emailRepository;
        private readonly IEmailRequestValidator _emailValidator;
        private readonly IEmailSender _emailSender;

        public SendEmailController(
            IEmailRepository emailRepository,
            IEmailRequestValidator emailValidator,
            IEmailSender emailSender,
            IRestrictedModificationChecker restrictedModificationChecker)
            : base(restrictedModificationChecker)
        {
            _emailRepository = emailRepository ?? throw new ArgumentNullException(nameof(emailRepository));
            _emailValidator = emailValidator ?? throw new ArgumentNullException(nameof(emailValidator));
            _emailSender = emailSender ?? throw new ArgumentNullException(nameof(emailSender));
        }

        [HttpPost("{id}/send")]
        public async Task<IActionResult> SendEmail(string id)
        {
            if (!_emailValidator.ValidateId(id)) return InvalidEmailId();

            Email email = await _emailRepository.Get(id);
            if (email == null) return EmailNotFound();
            
            if (IsModificationRestricted(email)) return AlreadySentError();

            SendingEmailResult sendingEmailResult = await _emailSender.Send(email);
            if (sendingEmailResult.Success == false) return InternalServerError();

            return Ok(email.ToEmailResponse());
        }
    }
}