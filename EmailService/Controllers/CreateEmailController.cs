﻿using EmailService.Entities;
using EmailService.Mappers;
using EmailService.Messages;
using EmailService.Repositories;
using EmailService.ResultObjects;
using EmailService.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace EmailService.Controllers
{
    [Route("api/email")]
    [ApiController]
    public class CreateEmailController : ControllerBase
    {
        private readonly IEmailRepository _emailRepository;
        private readonly IEmailRequestValidator _emailValidator;

        public CreateEmailController(
            IEmailRepository emailRepository,
            IEmailRequestValidator emailValidator)
        {
            _emailRepository = emailRepository ?? throw new ArgumentNullException(nameof(emailRepository));
            _emailValidator = emailValidator ?? throw new ArgumentNullException(nameof(emailValidator));
        }

        [HttpPost]
        public async Task<IActionResult> CreatePendingEmail([FromBody] EmailDto emailRequest)
        {
            EmailValidationResult emailValidationResult = _emailValidator.Validate(emailRequest);
            if (!emailValidationResult.Success) return BadRequest(emailValidationResult.ToErrorResponse());

            Email email = emailRequest.ToEmailEntity(SendingStatus.Pending);
            await _emailRepository.Add(email);
            
            return Created(GetCreatedItemUri(email), email.ToEmailResponse());
        }

        private Uri GetCreatedItemUri(Email email)
        {
            return new Uri($"{Request.Path}/{email.Id}", UriKind.Relative);
        }
    }
}