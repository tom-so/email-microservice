﻿using System;
using System.Threading.Tasks;
using EmailService.Entities;
using EmailService.Mappers;
using EmailService.Messages;
using EmailService.Repositories;
using EmailService.ResultObjects;
using EmailService.Services;
using EmailService.Services.EmailDetailValidators;
using Microsoft.AspNetCore.Mvc;

namespace EmailService.Controllers
{
    [Route("api/email")]
    [ApiController]
    public class ModifyEmailController : EmailModificationBaseController
    {
        private readonly IEmailRepository _emailRepository;
        private readonly IEmailModificationRepository _emailModificationRepository;
        private readonly IEmailRequestValidator _emailValidator;

        public ModifyEmailController(
            IEmailRepository emailRepository,
            IEmailModificationRepository emailModificationRepository,
            IEmailRequestValidator emailValidator,
            IRestrictedModificationChecker restrictedModificationChecker)
            : base(restrictedModificationChecker)
        {
            _emailRepository = emailRepository ?? throw new ArgumentNullException(nameof(emailRepository));
            _emailModificationRepository = emailModificationRepository ?? throw new ArgumentNullException(nameof(emailModificationRepository));
            _emailValidator = emailValidator ?? throw new ArgumentNullException(nameof(emailValidator));
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> ModifyEmail(string id, [FromBody] EmailDto emailRequest)
        {
            if (!_emailValidator.ValidateId(id)) return InvalidEmailId();

            Email email = await _emailRepository.Get(id);
            if (email == null) return EmailNotFound();

            if (IsModificationRestricted(email)) return AlreadySentError();

            EmailValidationResult emailValidationResult = _emailValidator.Validate(emailRequest);
            if (!emailValidationResult.Success) return BadRequest(emailValidationResult.ToErrorResponse());

            Email emailToModification = GetEmailToModification(id, emailRequest);
            Email modifiedEmail = await _emailModificationRepository.Modify(id, emailToModification);

            return Ok(modifiedEmail.ToEmailResponse());
        }

        private Email GetEmailToModification(string id, EmailDto emailToModification)
        {
            emailToModification.Id = id;
            return emailToModification.ToEmailEntity(SendingStatus.Pending);
        }
    }
}